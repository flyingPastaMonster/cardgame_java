package kartenspiel;

import java.util.Random;

public class Kartenspiel {
	private Spielkarte[] kartenspiel;
	private static int shuffle = 1000;
	
	
	public Kartenspiel(){
		this.kartenspiel = new Spielkarte[32];
		generateGame();
	}
	
	public Spielkarte getKarte(int i){
		return this.kartenspiel[i];
	}
	
	private void generateGame(){
		int i = 0;
		for (Farbe f: Farbe.values()){
			for (Wert w: Wert.values()){
				this.kartenspiel[i] = new Spielkarte(f,w);
				i++;
			}
		}
	}
	
	public String toString(){
		String s = "";
		for (Spielkarte sk : this.kartenspiel){
			s += sk.toString() + "\n";
		}
		return s;
	}
	
	public void mischen(){
		Random r = new Random();
		int i,x,y;
		Spielkarte t;
		for(i=0; i<shuffle; i++){
			x=r.nextInt(this.kartenspiel.length);
			y=r.nextInt(this.kartenspiel.length);
			t = this.kartenspiel[x];
			this.kartenspiel[x] = this.kartenspiel[y];
			this.kartenspiel[y] = t;
		}
	}
}
