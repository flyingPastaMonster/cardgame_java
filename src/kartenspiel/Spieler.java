package kartenspiel;

public class Spieler {
	private String name;
	private Spielkarte[] karten;
	private int current;
	private int score;
	private int wins;
	
	public int getWins() {
		return wins;
	}

	public void addWin() {
		this.wins++;
	}

	@Override
	public String toString() {
		return name + "(" + wins + ")" + ", " + score;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}
	
	public void addScore(int score) {
		this.score += score;
	}

	public String getName() {
		return name;
	}

	public Spieler(String name) {
		this.name = name;
		this.karten = new Spielkarte[16];
		this.current = 0;
		this.score = 0;
		this.wins = 0;
	}
	

	public void nimmKarteAuf(Spielkarte karte){
		this.karten[this.current] = karte;
		this.current++;
	}
	
	public Spielkarte legeKarteAb(){
		this.current--;
		Spielkarte t = this.karten[this.current];
		this.karten[this.current] = null;
		return t;
	}
}
