package kartenspiel;


public class Spielkarte implements Comparable<Spielkarte> {
	private Farbe farbe;
	private Wert wert;
	
	public Spielkarte(Farbe farbe, Wert wert) {
		this.farbe = farbe;
		this.wert = wert;
	}

	public Farbe getFarbe() {
		return farbe;
	}

	public Wert getWert() {
		return wert;
	}

	@Override
	public String toString() {
		return farbe + "(" + wert + ")";
	}

	@Override
	public int compareTo(Spielkarte sp) {
		if(this.farbe.ordinal() > sp.getFarbe().ordinal()){
			return 1;
		}else if (this.farbe.ordinal() == sp.getFarbe().ordinal()){
			return this.wert.ordinal() > sp.getWert().ordinal() ? 1 : 0;
		}
		return 0;
	}
}
