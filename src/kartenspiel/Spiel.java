package kartenspiel;

public class Spiel {
	private Kartenspiel karten;
	private Spieler spieler[];
	
	public Spiel(Kartenspiel karten, Spieler s1, Spieler s2) {
		this.karten = karten;
		Spieler spieler[] = {s1, s2};
		this.spieler = spieler;
	}
	
	private void verteileKarten(){
		for (int i=0; i<32; i++){
			this.spieler[i%this.spieler.length].nimmKarteAuf(karten.getKarte(i));
		}
	}
	
	public Spieler spiele(){
		for (Spieler sp: this.spieler){
			sp.setScore(0);
		}
		this.karten.mischen();
		this.verteileKarten();
		String match="";
		for(int i=0; i < 16; i++){
			Spielkarte sk1 = this.spieler[0].legeKarteAb(),
					sk2 = this.spieler[1].legeKarteAb();
			int s = sk1.compareTo(sk2);
			this.spieler[0].addScore(s);
			this.spieler[1].addScore((s+1)%2);
			match += sk1.toString() + " vs " + sk2.toString() + "\n" + this.gameState(s) + "\n";		
		}
		
		Spieler winner = null;
		if (this.spieler[0].getScore() > this.spieler[1].getScore()){
			winner = this.spieler[0];
		}else if (this.spieler[1].getScore() > this.spieler[0].getScore()){
			winner = this.spieler[1];
		}
		if (winner != null){
			winner.addWin();
			match += winner.getName() + " hat gewonnen!";
		}else{
			match += "Unentschieden!";
		}
		System.out.println(match + "\n");
		return winner;
	}
	
	private String gameState(int sc){
		String s="==> " + this.spieler[(sc+1) % 2].getName() + " won!";
		
		s += "\t\tStand: ";
		for (Spieler sp: this.spieler){
			s += sp.toString() + "\t";
		}
		return s;
	}
}
